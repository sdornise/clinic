import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import localeHe from '@angular/common/locales/he';
import {registerLocaleData} from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleEditComponent } from './articles/article-edit.component';
import { MainComponent } from './main/main.component';
import { HttpClientModule } from '@angular/common/http';

import { CategoryNamePipe } from './filters/category-name.pipe';
import { EnumToArrayPipe } from './filters/enum-to-array.pipe';
import { SexNamePipe } from './filters/sex-name.pipe';
import { RelationNamePipe } from './filters/relation-name.pipe';
import { ArticleNewComponent } from './articles/article-new.component';
import { ArticleReadComponent } from './articles/article-read.component';
import { ArticleCategoryComponent } from './articles/article-category.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { AppointmentNewComponent } from './appointments/appointment-new.component';
import { GuidanceStartComponent } from './appointments/guidance-start.component';
import { InternalComponent } from './main/internal.component';
import { StaffStatusNamePipe } from './filters/staff-status-name.pipe';
import { AppStatusNamePipe } from './filters/app-status-name.pipe';
import {ArticleReadByCategoryComponent} from './articles/article-read-by-category.component';

registerLocaleData(localeHe, 'he-HE');

@NgModule({
  declarations: [
    AppComponent,
    ArticlesComponent,
    ArticleReadComponent,
    ArticleCategoryComponent,
    ArticleReadByCategoryComponent,
    ArticleEditComponent,
    ArticleNewComponent,
    AppointmentsComponent,
    AppointmentNewComponent,
    GuidanceStartComponent,
    MainComponent,
    InternalComponent,
    CategoryNamePipe,
    EnumToArrayPipe,
    SexNamePipe,
    RelationNamePipe,
    StaffStatusNamePipe,
    AppStatusNamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ { provide: LOCALE_ID, useValue: 'he-HE' } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
