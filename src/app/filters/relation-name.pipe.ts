import { Pipe, PipeTransform } from '@angular/core';
import {Relation} from '../models/enums';

@Pipe({ name: 'relationName' })
export class RelationNamePipe implements PipeTransform {
  private names = {};
  constructor() {
    this.names['Mother'] = 'אם';
    this.names['Father'] = 'אב';
    this.names['Guardian'] = 'אופוטרופוס';
    this.names['Other'] = 'אחר';
  }

  transform(data: Object) {
    const name = this.names[typeof data === 'string' ? data : Relation[<string>data]];
    if (!name) {
      return data;
    }
    return name;
  }
}
