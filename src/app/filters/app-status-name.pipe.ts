import { Pipe, PipeTransform } from '@angular/core';
import {AppStatus, StaffStatus} from '../models/enums';

@Pipe({ name: 'AppStatusName' })
export class AppStatusNamePipe implements PipeTransform {
  private names = {};
  constructor() {
    this.names['Appointment'] = 'קליטה';
    this.names['Incomplete'] = 'שיבוץ';
    this.names['Recepted'] = 'התקבל';
    this.names['Rejected'] = 'נדחה';
    this.names['Pending'] = 'בהמתנה';
    this.names['Reviewed'] = 'בבדיקה';
    this.names['Canceled'] = 'מחוק';
    this.names['Other'] = 'אחר';
  }

  transform(data: Object) {
    const name = this.names[typeof data === 'string' ? data : AppStatus[<string>data]];
    if (!name) {
      return data;
    }
    return name;
  }
}
