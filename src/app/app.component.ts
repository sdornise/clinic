import { Component, OnInit } from '@angular/core';
import { ArticlesService } from './services/articles.service';
import { Article } from './models/article.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Clinic';
  today: number = Date.now();
  private articles: Article[];
  private treatmentArticles: Article[];
  private contactUsText: string;

  constructor(private articlesService: ArticlesService) { }

  ngOnInit() {
    this.articlesService.getAllArticles().subscribe((data: Article[]) => {
        this.articles = this.sort(data);
    });
    this.articlesService.getAllArticlesByCategory('איך מגיעים').subscribe((data: Article[]) => {
        this.contactUsText = data.length > 0 ? data[0].subTitle : null;
    });
    this.articlesService.getAllArticlesByCategory('טיפולים').subscribe((data: Article[]) => {
        this.treatmentArticles = this.sort(data);
    });
  }

  private sort(data) {
    return data.sort((a, b) => a.priority - b.priority);
  }
}
