import { Pipe, PipeTransform } from '@angular/core';
import {Sex} from '../models/enums';

@Pipe({ name: 'sexName' })
export class SexNamePipe implements PipeTransform {
  private names = {};
  constructor() {
    this.names['male'] = 'זכר';
    this.names['female'] = 'נקבה';
  }

  transform(data: Object) {
    const name = this.names[typeof data === 'string' ? data : Sex[<string>data]];
    if (!name) {
      return data;
    }
    return name;
  }
}
