import {AppStatus, Relation} from './enums';
import {Child} from './child.model';
import {Hmo} from './hmo.model';
import {Appointment} from './appointment.model';
import {Staff} from './staff.model';

export class AppDetails {
  id: string;
  childFkID: string;
  childFk: Child;
  hmoFkID: number;
  hmoFk: Hmo;
  appReasones: string;
  appFkID: string;
  appFk: Appointment;
  appRelation: Relation;
  recpDate: Date;
  appDate: Date;
  staffFkID: string;
  staffFk: Staff;
  aprDate: Date;
  // aprTime: Time; (aprDate includes that part)
  appStatus: AppStatus;
}
