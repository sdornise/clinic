import {ContactStatus} from './enums';

export class Hmo {
  id: number;
  name: string;
  contactName: string;
  email: string;
  phoneNum: string;
  contactStatus: ContactStatus;
  comments: string;
}
