import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Article} from '../models/article.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ArticlesService {
  private static BASE = '/api/ArticlesApi1';

  constructor(private http: HttpClient) { }

  getAllArticles() {
    return this.http.get(ArticlesService.BASE);
  }

  getArticle(id: string) {
    return this.http.get(`${ArticlesService.BASE}/${id}`);
  }

  update(article: Article) {
    return this.http.put(`${ArticlesService.BASE}/${article.id}`, article, httpOptions);
  }

  create(article: Article) {
    return this.http.post(ArticlesService.BASE, article, httpOptions);
  }

  getAllArticlesByCategory(catName: string) {
    return this.http.get(`${ArticlesService.BASE}/ByCategoryName/${catName}`);
  }
}
