import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppDetails} from '../models/app-details.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AppDetailsService {
  private static BASE = '/api/AppDetailsApi';

  constructor(private http: HttpClient) { }

  getAllAppDetails() {
    return this.http.get(AppDetailsService.BASE);
  }

  getAppDetails(id: number) {
    return this.http.get(`${AppDetailsService.BASE}/${id}`);
  }

  update(appDetails: AppDetails) {
    return this.http.put(`${AppDetailsService.BASE}/${appDetails.id}`, appDetails, httpOptions);
  }

  create(appDetails: AppDetails) {
    return this.http.post(AppDetailsService.BASE, appDetails, httpOptions);
  }

  delete(adID: string) {
    return this.http.delete(`${AppDetailsService.BASE}/${adID}`, httpOptions);
  }
}
