import {Component, OnInit} from '@angular/core';
import {AppDetails} from '../models/app-details.model';
import {AppStatus} from '../models/enums';
import {Child} from '../models/child.model';
import {Staff} from '../models/staff.model';
import {Hmo} from '../models/hmo.model';
import {AppDetailsService} from '../services/app-details.service';
import {AppointmentsService} from '../services/appointments.service';
import {ChildrenService} from '../services/children.service';
import {StaffService} from '../services/staff.service';
import {HmosService} from '../services/hmos.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {
  private AppStatus: typeof AppStatus = AppStatus;
  private children: Child[];
  private staff: Staff[];
  private hmos: Hmo[];
  private appDetails: AppDetails[];
  private displayedAppDetails: AppDetails[];

  private recpDateFilter: number;
  private childFilter: string;
  private appStatusFilter: AppStatus;
  private aprDateFilter: number;
  private staffFilter: string;
  private hmoFilter: number;

  constructor(
      private appDetailsService: AppDetailsService,
      private appointmentsService: AppointmentsService,
      private childrenService: ChildrenService,
      private staffService: StaffService,
      private hmosService: HmosService,
  ) {
    this.recpDateFilter = undefined;
    this.childFilter = undefined;
    this.appStatusFilter = undefined;
    this.aprDateFilter = undefined;
    this.staffFilter = undefined;
    this.hmoFilter = undefined;

    this.childrenService.getAllChildren().subscribe((data: Child[]) => {
      this.children = data;
    });

    this.staffService.getAllStuff().subscribe((data: Staff[]) => {
      this.staff = data;
    });

    this.hmosService.getAllHmos().subscribe((data: Hmo[]) => {
      this.hmos = data;
    });
  }

  ngOnInit() {
    this.appDetailsService.getAllAppDetails().subscribe((data: AppDetails[]) => {
      this.appDetails = data;
      this.refreshDisplayedArticles();
      }
    );
  }

  onRecpDateInput(value: Date): void {
    const date = new Date(value);
    date.setHours(0, 0, 0, 0);
    this.recpDateFilter = date.getTime();
    this.refreshDisplayedArticles();
  }

  onIDnetInput(value: string): void {
    this.childFilter = value !== '' ? value : undefined;
    this.refreshDisplayedArticles();
  }

  onAppStatusInput(value: number) {
    this.appStatusFilter = value;
    this.refreshDisplayedArticles();
  }

  onAprDateFilterInput(value: Date): void {
    const date = new Date(value);
    date.setHours(0, 0, 0, 0);
    this.aprDateFilter = date.getTime();
    this.refreshDisplayedArticles();
  }

  onStaffFilterInput(value: string): void {
    this.staffFilter = value !== '' ? value : undefined;
    this.refreshDisplayedArticles();
  }

  onHmoFilterInput(value: number): void {
    this.hmoFilter = typeof value === 'string' ? parseInt(value, 10) : value;
    this.refreshDisplayedArticles();
  }

  private refreshDisplayedArticles() {
    this.displayedAppDetails = this.appDetails.filter(a => {
      if (this.recpDateFilter !== undefined && !isNaN(this.recpDateFilter) &&
          new Date(a.recpDate).getTime() !== this.recpDateFilter) {
        return false;
      }

      if (this.childFilter !== undefined && a.childFkID !== this.childFilter) {
        return false;
      }

      if (this.appStatusFilter !== undefined && ('' + this.appStatusFilter) !== '' &&
          AppStatus[a.appStatus] !== AppStatus[this.appStatusFilter]) {
        return false;
      }

      if (this.aprDateFilter !== undefined && !isNaN(this.aprDateFilter) &&
          new Date(a.aprDate).getTime() !== this.aprDateFilter) {
        return false;
      }

      if (this.staffFilter !== undefined && a.staffFkID !== this.staffFilter) {
        return false;
      }

      if (this.hmoFilter !== undefined && a.hmoFkID !== this.hmoFilter) {
        return false;
      }

      return true;
    });
  }

  getFormatedDate(dateStr: string) {
    function pad(n: string) {
      return n.length < 2 ? '0' + n : n;
    }
    if (!dateStr) {
      return undefined;
    }
    const date = new Date(dateStr);
    const month: string = pad('' + (date.getMonth() + 1));
    const day: string = pad('' + date.getDate());
    const hours: string = pad('' + date.getHours());
    const minutes: string = pad('' + date.getMinutes());

    return `${date.getFullYear()}-${month}-${day}T${hours}:${minutes}`;
  }

  onDateInput(ad: AppDetails, field: string, value: string): void {
    ad[field] = new Date(value).getTime();
  }

  delete(adID: string) {
    if (confirm('האם אתה בטוח שברצונך למחוק פגישה זו?')) {
      this.appDetailsService.delete(adID).subscribe(() => {
          this.appDetails = this.appDetails.filter(item => {
            return item.id !== adID;
          });
          this.refreshDisplayedArticles();
        },
        this.onError
      );
    }
  }

  update(ad: AppDetails) {
    this.appDetailsService.update(ad).subscribe(
      () => {
        this.refreshDisplayedArticles();
      },
      this.onError);
  }

  onError(error) {
    if (error.status === 401) {
      window.location.href = 'https://localhost:44333/login';
    } else {
      alert(`Error!\nStatus code: ${error.status}\n${error.statusText}`);
    }
  }
}
