import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {Category} from '../models/category.model';
import {CategoriesService} from '../services/categories.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HmosService} from "../services/hmos.service";

@Component({
  selector: 'app-article-new',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleNewComponent implements OnInit {
  private article: Article;
  private categories: Category[];

  constructor(private route: ActivatedRoute,
              private articlesService: ArticlesService,
              private categoriesService: CategoriesService,
              private router: Router) {}

  ngOnInit() {
    this.article = new Article();
    this.categoriesService.getAllCategories().subscribe((data: Category[]) => {
      this.categories = data;
    });
  }

  save() {
    this.articlesService.create(this.article)
      .subscribe(() => this.exit());
  }

  exit() {
    this.router.navigate(['/']);
  }
}
