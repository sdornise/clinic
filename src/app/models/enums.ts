export enum Sex {
  male = 0,
  female = 1
}

export enum Relation {
    // [Display(Name = "אם")]
  Mother = 0,
    // [Display(Name = "אב")]
  Father = 1,
    // [Display(Name = "אופוטרופוס")]
  Guardian = 2,
    // [Display(Name = "אחר")]
  Other = 3
}

export enum AppStatus {
    // [Display(Name = "קליטה")]
  Appointment = 0,
    // [Display(Name = "שיבוץ")]
  Incomplete = 1,
    // [Display(Name = "התקבל")]
  Recepted = 2,
    // [Display(Name = "נדחה")]
  Rejected = 3,
    // [Display(Name = "בהמתנה")]
  Pending = 4,
    // [Display(Name = "בבדיקה")]
  Reviewed = 5,
    // [Display(Name = "מחוק")]
  Canceled = 6,
    // [Display(Name = "אחר")]
  Other = 7
}

export enum ContactStatus {
  NA = 0,
  Available = 1
}

export enum Relations {
  father = 0,
  mother = 1
}

export enum Role {
    // [Display(Name = "מועמד")]
  Candidate = 0,
    // [Display(Name = "מטפל")]
  Therapist = 1,
    // [Display(Name = "קלינאי תקשורת")]
  SpeechTherapist = 2,
    // [Display(Name = "מרפא בעיסוק")]
  OccupationalTherapist = 3,
    // [Display(Name = "מזכיר")]
  Secretary = 4,
    // [Display(Name = "מנהל")]
  Manager = 5,
    // [Display(Name = "אדמיניסטרטור")]
  Admin = 6,
    // [Display(Name = "אחר")]
  Other = 7
}

export enum StaffStatus {
    // [Display(Name = "מועמד")]
  Candidate = 0,
    // [Display(Name = "מועסק")]
  Employed = 1,
    // [Display(Name = "חופשת לידה")]
  MaternityLeave = 2,
    // [Display(Name = "חופשה")]
  Vacation = 3,
    // [Display(Name = "חל\"ת")]
  UnpaidLeave = 4,
    // [Display(Name = "אחר")]
  Other = 5,
    // [Display(Name = "מבוטל")]
  Delete = 6
}
