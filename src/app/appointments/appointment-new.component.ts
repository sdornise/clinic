import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Appointment} from '../models/appointment.model';
import {Child} from '../models/child.model';
import {Sex, Relations} from '../models/enums';
import {Hmo} from '../models/hmo.model';
import {HmosService} from '../services/hmos.service';
import {AppDetails} from '../models/app-details.model';
import {AppDetailsService} from '../services/app-details.service';
import {ArticlesService} from '../services/articles.service';
import {ArticleReadByCategoryComponent} from '../articles/article-read-by-category.component';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointment-new.component.html',
  styleUrls: ['./appointment-new.component.css']
})
export class AppointmentNewComponent extends ArticleReadByCategoryComponent implements OnInit {
  private Sex: typeof Sex = Sex;
  private Relations: typeof Relations = Relations;
  private appDetails: AppDetails;
  private hmos: Hmo[];
  private part1: boolean;
  private part2: boolean;
  private part3: boolean;
  private currentPart: number;

  constructor(private appDetailsService: AppDetailsService,
              private hmosService: HmosService,
              private router: Router,
              private articlesService1: ArticlesService // extends ArticleReadByCategoryComponent
  ) {
    super(articlesService1, 'סיום זימון');
    this.currentPart = 1;
    this.part1 = true;
    this.part2 = false;
    this.part3 = false;

    this.hmosService.getAllHmos().subscribe((data: Hmo[]) => {
      this.hmos = data;
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.appDetails = new AppDetails();
    this.appDetails.appFk = new Appointment();
    this.appDetails.childFk = new Child();
    this.refreshParts();
  }

  onDateInput(objStr: string, field: string, value: string): void {
    const obj: Object = objStr ? this.appDetails[objStr] : this.appDetails;
    if (!obj) {
      return;
    }
    obj[field] = new Date(value).getTime();
  }

  save() {
    this.appDetailsService.create(this.appDetails)
      .subscribe(() => this.exit(),
      this.onError);
  }

  onError(error) {
    if (error.status === 401) {
      window.location.href = 'https://localhost:44333/login';
    } else {
      alert(`Error!\nStatus code: ${error.status}\n${error.statusText}`);
    }
  }

  next() {
    this.currentPart++;
    this.refreshParts();
  }

  back() {
    this.currentPart--;
    if (this.currentPart < 1) {
      this.router.navigate(['/appointments/guidance']);
      return;
    }
    this.refreshParts();
  }

  private refreshParts() {
    this.part1 = this.currentPart === 1;
    this.part2 = this.currentPart === 2;
    this.part3 = this.currentPart === 3;
  }

  exit() {
    this.router.navigate(['/']);
  }
}
