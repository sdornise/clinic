import { Component, OnInit } from '@angular/core';
import {ArticleReadByCategoryComponent} from '../articles/article-read-by-category.component';
import {ActivatedRoute} from '@angular/router';
import {ArticlesService} from '../services/articles.service';

@Component({
  selector: 'app-internal',
  templateUrl: './internal.component.html'
})
export class InternalComponent {

  constructor() { }

}
