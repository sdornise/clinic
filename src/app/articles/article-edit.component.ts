import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {Category} from '../models/category.model';
import {CategoriesService} from '../services/categories.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {
  private article: Article;
  private categories: Category[];
  private id: string;

  constructor(private route: ActivatedRoute,
              private articlesService: ArticlesService,
              private categoriesService: CategoriesService,
              private router: Router) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.articlesService.getArticle(this.id).subscribe((data: Article) => {
      this.article = data;
      }
    );
    this.categoriesService.getAllCategories().subscribe((data: Category[]) => {
      this.categories = data;
    });
  }

  save() {
    this.articlesService.update(this.article)
      .subscribe(
        () => this.exit(),
        this.onError);
  }

  exit() {
    this.router.navigate(['/']);
  }

  onError(error) {
    if (error.status === 401) {
      window.location.href = 'https://localhost:44333/login';
    } else {
      alert(`Error!\nStatus code: ${error.status}\n${error.statusText}`);
    }
  }
}
