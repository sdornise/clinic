import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HmosService {

  constructor(private http: HttpClient) { }

  getAllHmos() {
    return this.http.get('/api/HMOsApi');
  }
}
