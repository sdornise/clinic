import { Pipe, PipeTransform } from '@angular/core';
import {StaffStatus} from '../models/enums';

@Pipe({ name: 'staffStatusName' })
export class StaffStatusNamePipe implements PipeTransform {
  private names = {};
  constructor() {
    this.names['Candidate'] = 'מועמד';
    this.names['Employed'] = 'מועסק';
    this.names['MaternityLeave'] = 'חופשת לידה';
    this.names['Vacation'] = 'חופשה';
    this.names['UnpaidLeave'] = 'חל\\"ת';
    this.names['Other'] = 'אחר';
    this.names['Delete'] = 'מבוטל';
  }

  transform(data: Object) {
    const name = this.names[typeof data === 'string' ? data : StaffStatus[<string>data]];
    if (!name) {
      return data;
    }
    return name;
  }
}
