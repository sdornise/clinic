import { Pipe, PipeTransform } from '@angular/core';
import { HmosService } from '../services/hmos.service';
import { Hmo } from '../models/hmo.model';


@Pipe({name: 'hmoName'})
export class HmoNamePipe implements PipeTransform {
  private hmos: Hmo[];
  private ready: Promise<Object>;

  constructor(private hmosService: HmosService) {
    this.ready = new Promise((resolve, reject) => {
      this.hmosService.getAllHmos().subscribe((data: Hmo[]) => {
        this.hmos = data;
        resolve();
      });
    });
  }

  transform(id: number): Promise<string> {
    return this.ready.then(() => {
      const anies: Hmo[] = this.hmos.filter((h: Hmo) => h.id === id);
      return anies.length > 0 ? anies[0].name : '';
    });
  }
}
