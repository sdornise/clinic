import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './article-read.component.html',
  styleUrls: ['./article-read.component.css']
})
export class ArticleReadComponent implements OnInit {
  protected article: Article;
  protected id: string;

  constructor(private route: ActivatedRoute,
              private articlesService: ArticlesService) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.articlesService.getArticle(this.id).subscribe((data: Article) => {
        this.article = data;
      }
    );
  }
}
