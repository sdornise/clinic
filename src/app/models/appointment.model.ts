export class Appointment {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phoneNum: string;
  comments: string;
}
