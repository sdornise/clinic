import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {Category} from '../models/category.model';
import {CategoriesService} from '../services/categories.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  private articles: Article[];
  private displayedArticles: Article[];
  private categories: Category[];

  private categoryFilter: string;
  private authorFilter: string;
  private pickedDate: number;
  private titleFilter: string;
  private sortField: string;

  private isSortingByTitle: boolean;
  private isSortingByAuthor: boolean;
  private isSortingByPublishDate: boolean;
  private isSortingByCategory: boolean;
  private isSortingByPriority: boolean;
  private ascendingSort: boolean;
  private arrowDirection: string;

  constructor(private articlesService: ArticlesService,
              private categoriesService: CategoriesService,
              private router: Router) {
    this.categoryFilter = undefined;
    this.authorFilter = '';
    this.pickedDate = undefined;
    this.titleFilter = '';

    this.categoriesService.getAllCategories().subscribe((data: Category[]) => {
      this.categories = data;
    });

    this.ascendingSort = true;
    this.arrowDirection = 'up';
  }

  ngOnInit() {
    this.articlesService.getAllArticles().subscribe((data: Article[]) => {
      this.articles = data;
      this.refreshDisplayedArticles();
      }
    );
  }

  onCategoryInput(value: string) {
    this.categoryFilter = value !== '' ? value : undefined;
    this.refreshDisplayedArticles();
  }

  onDateInput(value: Date): void {
    const date = new Date(value);
    date.setHours(0, 0, 0, 0);
    this.pickedDate = date.getTime();
    this.refreshDisplayedArticles();
  }

  onTitleInput(value: string) {
    this.titleFilter = value;
    this.refreshDisplayedArticles();
  }

  onAuthorInput(value: string) {
    this.authorFilter = value;
    this.refreshDisplayedArticles();
  }

  sortBy(value: string) {
    if (this.sortField === value) {
      this.ascendingSort = !this.ascendingSort;
    } else {
      this.sortField = value;
    }
    this.arrowDirection = this.ascendingSort ? 'up' : 'down';
    this.isSortingByTitle = (value === 'title');
    this.isSortingByAuthor = (value === 'author');
    this.isSortingByPublishDate = (value === 'publishDate');
    this.isSortingByCategory = (value === 'category');
    this.isSortingByPriority = (value === 'priority');
    this.refreshDisplayedArticles();
  }

  private refreshDisplayedArticles() {
    this.displayedArticles = this.articles.filter(a => {
      if (this.pickedDate !== undefined && a.publishDate.getTime() !== this.pickedDate) {
        return false;
      }

      if (this.categoryFilter !== undefined && a.categoryFkID !== this.categoryFilter) {
        return false;
      }

      if (this.titleFilter !== '' && !a.title.includes(this.titleFilter)) {
        return false;
      }

      if (this.authorFilter !== '' && !a.author.includes(this.authorFilter)) {
        return false;
      }

      return true;
    }).sort((a1, a2) => {
      const a1f = a1[this.sortField];
      const a2f = a2[this.sortField];
      if (a2f === a1f) {
        return 0;
      }
      if (this.ascendingSort) {
        return a2f < a1f ? 1 : -1;
      } else {
        return a2f < a1f ? -1 : 1;
      }
    });
  }

  createNewArticle() {
    this.router.navigate(['/articles/new']);
  }
}
