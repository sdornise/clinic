import { Injectable } from '@angular/core';
import {Appointment} from '../models/appointment.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AppointmentsService {
  private static BASE = '/api/AppointmentsApi';

  constructor(private http: HttpClient) { }

  create(appointment: Appointment) {
    return this.http.post(AppointmentsService.BASE, appointment, httpOptions);
  }

  getAllAppointments() {
    return this.http.get(AppointmentsService.BASE);
  }
}
