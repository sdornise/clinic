import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'enumToArray' })
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    const keys = Object.keys(data);
    // return keys.slice(keys.length / 2);
    let keyValues = [];
    for (let i = 0; i < keys.length / 2; i++) {
      const id = keys[i];
      keyValues.push({ key: id, value: data[id]});
    }

    return keyValues;
  }
}
