import { Component, OnInit } from '@angular/core';
import {ArticleReadByCategoryComponent} from '../articles/article-read-by-category.component';
import {ActivatedRoute} from '@angular/router';
import {ArticlesService} from '../services/articles.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent extends ArticleReadByCategoryComponent implements OnInit {

  constructor(private articlesService1: ArticlesService /* extends ArticleReadByCategoryComponent */) {
    super(articlesService1, 'דף ראשי');
  }

  ngOnInit() {
    super.ngOnInit();
  }

}
