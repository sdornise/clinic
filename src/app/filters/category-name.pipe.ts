import { Pipe, PipeTransform } from '@angular/core';
import { CategoriesService } from '../services/categories.service';
import { Category } from '../models/category.model';


@Pipe({name: 'categoryName'})
export class CategoryNamePipe implements PipeTransform {
  private categories: Category[];
  private ready: Promise<Object>;

  constructor(private categoriesService: CategoriesService) {
    this.ready = new Promise((resolve, reject) => {
      this.categoriesService.getAllCategories().subscribe((data: Category[]) => {
        this.categories = data;
        resolve();
      });
    });
  }

  transform(id: string): Promise<string> {
    return this.ready.then(() => {
      const anies: Category[] = this.categories.filter((c: Category) => c.id === id);
      return anies.length > 0 ? anies[0].catName : '';
    });
  }
}
