import { Injectable } from '@angular/core';
import {Child} from '../models/child.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ChildrenService {
  private static BASE = '/api/ChildrenApi';

  constructor(private http: HttpClient) { }

  create(child: Child) {
    return this.http.post(ChildrenService.BASE, child, httpOptions);
  }

  getAllChildren() {
    return this.http.get(ChildrenService.BASE);
  }
}
