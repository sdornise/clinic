import {Component} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleReadComponent} from '../articles/article-read.component';
import {ArticleReadByCategoryComponent} from "../articles/article-read-by-category.component";

@Component({
  selector: 'app-guidance-start',
  templateUrl: './guidance.component.html',
  styleUrls: ['./guidance.component.css']
})
export class GuidanceStartComponent extends ArticleReadByCategoryComponent {

  constructor(private articlesService1: ArticlesService,
              private router: Router) {
    super(articlesService1, 'זימון תור');
  }

  next() {
    this.router.navigate(['/appointments/new']);
  }

  exit() {
    this.router.navigate(['/']);
  }
}
