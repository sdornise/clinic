import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleEditComponent } from './articles/article-edit.component';
import { ArticleNewComponent } from './articles/article-new.component';
import { ArticleReadComponent } from './articles/article-read.component';
import { AppointmentNewComponent } from './appointments/appointment-new.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { GuidanceStartComponent } from './appointments/guidance-start.component';
import {InternalComponent} from './main/internal.component';
import {ArticleCategoryComponent} from './articles/article-category.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'articles/:id/read', component: ArticleReadComponent },
  { path: 'articles/:catName/category', component: ArticleCategoryComponent },
  { path: 'articles/:id/edit', component: ArticleEditComponent },
  { path: 'articles/new', component: ArticleNewComponent },
  { path: 'appointments', component: AppointmentsComponent },
  { path: 'appointments/new', component: AppointmentNewComponent },
  { path: 'appointments/guidance', component: GuidanceStartComponent },
  { path: 'internal', component: InternalComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
