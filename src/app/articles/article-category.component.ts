import {Component} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {ActivatedRoute} from '@angular/router';
import {ArticleReadByCategoryComponent} from './article-read-by-category.component';

@Component({
  selector: 'app-article-category',
  templateUrl: './article-category.component.html'
})
export class ArticleCategoryComponent extends ArticleReadByCategoryComponent {
  protected article: Article;
  protected catName: string;

  constructor(private route: ActivatedRoute,
              private articlesService1: ArticlesService) {
    super(articlesService1, route.snapshot.paramMap.get('catName'));
  }
}
