export class Article {
  id: string;
  author: string;
  publishDate: Date;
  title: string;
  subTitle: string;
  pictureAddr: string;
  artText: string;
  isApproved: boolean;
  priority: number;
  categoryFkID: string;
  comments: string;
}
