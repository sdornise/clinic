import {Sex} from './enums';

export class Child {
  id: string;
  iDnt: string
  firstName: string;
  lastName: string;
  sex: Sex;
  birthDate: Date;
  comments: string;

  constructor () {
    this.sex = Sex.male;
  }
}
