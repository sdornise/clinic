import {Role, StaffStatus} from './enums';

export class Staff {
  id: string;
  iDnt: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNum: string;
  role: Role;
  staffStatus: StaffStatus;
  comments: string;
  staffWeeklyReceptionList: Date[];
  fullName: string;

  constructor () { }
}
