import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './article-read.component.html',
  styleUrls: ['./article-read.component.css']
})
export class ArticleReadByCategoryComponent implements OnInit {
  protected article: Article;
  protected catName: string;

  constructor(private articlesService: ArticlesService,
              catName: string) {
    this.catName = catName;
  }

  ngOnInit() {
    this.articlesService.getAllArticlesByCategory(this.catName).subscribe((data: Article[]) => {
        this.article = data.length > 0 ? data[0] : null;
    });
  }
}
